import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { BrowserRouter, Route, Redirect } from "react-router-dom";
import "./App.css";
import { fetchUsers } from "./actions/actions";
import UsersPage from "./components/pages/UsersPage";
import Chat from "./components/pages/Chat";
import LoginPage from "./components/pages/LoginPage";

function App() {
  const isLogined = useSelector((state) => state.chat.currentUser.isLogined);
  const isAdmin = useSelector((state) => state.chat.currentUser.isAdmin);

  console.log("isAdmin", isAdmin);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchUsers());
  });

  return (
    <BrowserRouter>
      <Route path="/">
        {isLogined ? <Redirect to="/chat" /> : <Redirect to="/login" />}
      </Route>
      <Route path="/login">
        <LoginPage />
      </Route>
      <Route path="/chat">
        <Chat />
      </Route>
      {isAdmin ? (
        <Route path="/users">
          <UsersPage />
        </Route>
      ) : null}
    </BrowserRouter>
  );
}

export default App;
