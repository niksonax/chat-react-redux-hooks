const BASE_URL = "http://localhost:3001";

const getAll = async (path) => {
  const response = await fetch(`${BASE_URL}${path}`);
  const data = await response.json();
  return data;
};

const getMessages = async () => {
  return getAll("/messages");
};

const getUsers = async () => {
  return getAll("/users");
};

const deleteEntity = async (path, id) => {
  const response = await fetch(`${BASE_URL}${path}${id}`, {
    method: "DELETE",
  });
  const data = await response.json();
  return data;
};

const deleteUser = async (id) => {
  return deleteEntity("/users", `/${id}`);
};

export { getMessages, getUsers, deleteUser };
