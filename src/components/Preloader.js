import React from "react";
import "./Preloader.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

function Preloader() {
  return (
    <div className="preloader">
      <FontAwesomeIcon icon={faSpinner} className="icon" />
    </div>
  );
}

export default Preloader;
