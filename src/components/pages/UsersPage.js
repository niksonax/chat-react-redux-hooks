import React from "react";
import { useSelector, useDispatch } from "react-redux";
import "./UsersPage.css";
import { removeUser } from "../../actions/actions";

function User(props) {
  const dispatch = useDispatch();

  const onUserDelete = () => {
    dispatch(removeUser(props.id));
  };

  return (
    <div className="user-container">
      <div className="user-name">{props.name}</div>
      <button className="edit-user-btn">Edit</button>
      <button className="delete-user-btn" onClick={onUserDelete}>
        Delete
      </button>
    </div>
  );
}

function UsersPage() {
  const users = useSelector((state) => state.chat.users);

  const userElements = users.map((user) => {
    /* if (message.userId === props.userId) {
      return (
        <User
          key={message.id}
          id={message.id}
          text={message.text}
          createdAt={message.createdAt}
        />
      );
    } */
    return <User key={user.id} id={user.id} name={user.name} />;
  });

  return (
    <div className="users-page">
      <button className="add-user-btn">Add User</button>
      {userElements}
    </div>
  );
}

export default UsersPage;
