import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { setUser } from "../../actions/actions";
import "./LoginPage.css";

function LoginPage() {
  const users = useSelector((state) => state.chat.users);

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const dispatch = useDispatch();
  const history = useHistory();

  const onChangeUsername = (e) => {
    setUsername(e.target.value);
  };

  const onChangePassword = (e) => {
    setPassword(e.target.value);
  };

  const isUserValid = (name, pass) => {
    for (const user of users) {
      if (user.name === name && user.password === pass) return true;
    }
    return false;
  };

  const isUserAdmin = (name) => {
    for (const user of users) {
      if (user.name === name && user.type === "admin") return true;
    }
    return false;
  };

  const loginHandler = () => {
    if (username === "" || password === "") return;
    if (isUserValid(username, password)) {
      const isAdmin = isUserAdmin(username);
      dispatch(setUser(username, isAdmin, true));
      history.push("/chat");
    } else {
      alert("User with this credentials doesnt exists!");
    }
  };

  return (
    <div className="login-container">
      <input
        type="text"
        value={username}
        onChange={(e) => onChangeUsername(e)}
      ></input>
      <input
        type="password"
        value={password}
        onChange={(e) => onChangePassword(e)}
      ></input>
      <button onClick={loginHandler}>Log In</button>
    </div>
  );
}

export default LoginPage;
