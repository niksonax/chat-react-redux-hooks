import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import { v4 as uuidv4 } from "uuid";
import "./Chat.css";
import { setCurrentUserId, fetchMessages } from "../../actions/actions";
import Header from "../Header";
import MessageList from "../messages/MessageList";
import MessageInput from "../messages/MessageInput";
import Preloader from "../Preloader";
import EditMessageModal from "../messages/EditMessageModal";

const Chat = (props) => {
  const { users, messages, currentUserId, editModal, preloader } = useSelector(
    (state) => ({
      users: state.chat.users,
      currentUserId: state.chat.currentUser.id,
      messages: state.chat.messages,
      editModal: state.chat.editModal,
      preloader: state.chat.preloader,
    })
  );
  const dispatch = useDispatch();

  useEffect(() => {
    const setUserId = () => {
      const id = uuidv4();
      dispatch(setCurrentUserId(id));
    };

    dispatch(fetchMessages());
    setUserId();
  }, []);

  if (preloader) return <Preloader />;
  return (
    <div className="chat">
      <NavLink to="/users">Users</NavLink>
      <Header users={users} messages={messages} />
      {editModal ? <EditMessageModal /> : null}
      <MessageList userId={currentUserId} />
      <MessageInput userId={currentUserId} />
    </div>
  );
};

export default Chat;
