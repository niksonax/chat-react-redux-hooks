import React from "react";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import "./OwnMessage.css";
import {
  setMessages,
  setEditingMessageId,
  toggleEditModal,
} from "../../actions/actions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashAlt, faEdit } from "@fortawesome/free-solid-svg-icons";

function OwnMessage(props) {
  const messages = useSelector((state) => state.chat.messages);

  const dispatch = useDispatch();

  const onMessageEdit = () => {
    dispatch(setEditingMessageId(props.id));
    dispatch(toggleEditModal(true));
  };

  const onMessageDelete = () => {
    const newMessages = messages.filter((msg) => msg.id !== props.id);
    dispatch(setMessages(newMessages));
  };

  const date = new Date(props.createdAt);
  const time = moment(date).format("HH:mm");

  return (
    <div className="own-message">
      <div className="message-time">{time}</div>
      <div className="message-text">{props.text}</div>
      <div className="message-tools">
        <span className="message-edit" onClick={onMessageEdit}>
          <FontAwesomeIcon icon={faEdit} />
        </span>
        <span className="message-delete" onClick={onMessageDelete}>
          <FontAwesomeIcon icon={faTrashAlt} />
        </span>
      </div>
    </div>
  );
}

/* const mapStateToProps = (state) => {
  return {
    chat: state.chat,
  };
};

const mapDispatchToProps = {
  setMessages,
  setEditingMessageId,
  toggleEditModal,
}; */

export default OwnMessage;
