import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { v4 as uuidv4 } from "uuid";
import "./MessageInput.css";
import { setMessages } from "../../actions/actions";

function MessageInput(props) {
  const { messages, editModal, username } = useSelector((state) => ({
    messages: state.chat.messages,
    editModal: state.chat.editModal,
    username: state.chat.currentUser.name,
  }));
  const [message, setMessage] = useState("");

  const dispatch = useDispatch();

  const textarea = useRef();

  const createMessage = () => {
    return {
      id: uuidv4(),
      userId: props.userId,
      user: username,
      text: message,
      createdAt: new Date(Date.now()).toString(),
    };
  };

  const onChangeHandler = (e) => {
    const string = e.target.value;
    if (string.substr(string.length - 1) === "\n" && !e.shiftKey) return;
    setMessage(e.target.value);
  };

  const onClickHandler = () => {
    if (textarea.current) textarea.current.focus();
  };

  const onEnterHandler = (e) => {
    if (e.key === "Enter" && !e.shiftKey) {
      sendMessageHandler();
      onClickHandler();
    }
  };

  const sendMessageHandler = () => {
    if (message === "" || message === "\n") return;
    const newMessages = messages.slice();
    const newMessage = createMessage();
    newMessages.push(newMessage);
    dispatch(setMessages(newMessages));
    setMessage("");
  };

  useEffect(() => {
    window.addEventListener("keyup", onEnterHandler);

    return () => {
      window.removeEventListener("keyup", onEnterHandler);
    };
  });

  return (
    <div className="message-input">
      <textarea
        ref={textarea}
        className="message-input-text"
        value={message}
        onChange={(e) => onChangeHandler(e)}
        disabled={editModal}
      ></textarea>
      <button
        className="message-input-button"
        onClick={() => {
          sendMessageHandler();
          onClickHandler();
        }}
      >
        Send
      </button>
    </div>
  );
}

export default MessageInput;
