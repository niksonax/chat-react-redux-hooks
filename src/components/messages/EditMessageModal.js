import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./EditMessageModal.css";
import { toggleEditModal, setMessages } from "../../actions/actions";

function EditMessageModal() {
  const { messageId, messages, editModal } = useSelector((state) => ({
    messageId: state.chat.modal.messageId,
    messages: state.chat.messages,
    editModal: state.chat.editModal,
  }));

  const dispatch = useDispatch();

  const findMessageById = (id) => {
    if (id === 0) return { text: "" };
    for (const message of messages) {
      if (message.id === id) return message;
    }
  };

  const message = findMessageById(messageId);
  const [text, setText] = useState(message.text);

  const onEnterHandler = (e) => {
    if (e.key === "Enter" && !e.shiftKey) {
      onSubmitModal();
    }
  };

  useEffect(() => {
    window.addEventListener("keyup", onEnterHandler);

    return () => {
      window.removeEventListener("keyup", onEnterHandler);
    };
  });

  const onChangeText = (e) => {
    setText(e.target.value);
  };

  const onSubmitModal = () => {
    const updatedMessages = messages.slice();
    for (const message of updatedMessages) {
      if (message.id === messageId) {
        const updatedMessage = { ...message, text: text };
        const messageIndex = updatedMessages.indexOf(message);
        updatedMessages.splice(messageIndex, 1, updatedMessage);
      }
    }
    dispatch(setMessages(updatedMessages));
    onCloseModal();
  };

  const onCloseModal = () => {
    dispatch(toggleEditModal(false));
  };

  if (editModal === false) return null;

  return (
    <div className={`edit-message-modal ${editModal ? "modal-shown" : ""}`}>
      <textarea
        className="edit-message-input"
        value={text}
        onChange={onChangeText}
      ></textarea>
      <div className="edit-message-buttons">
        <button className="edit-message-close" onClick={onCloseModal}>
          Close
        </button>
        <button className="edit-message-button" onClick={onSubmitModal}>
          Edit
        </button>
      </div>
    </div>
  );
}

export default EditMessageModal;
