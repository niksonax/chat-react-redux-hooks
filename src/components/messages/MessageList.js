import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import "./MessageList.css";
import Message from "./Message";
import OwnMessage from "./OwnMessage";

function MessageList(props) {
  const messages = useSelector((state) => state.chat.messages);

  const messagesEndRef = React.createRef();

  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
  };

  useEffect(() => {
    scrollToBottom();
  });

  const sortedMessages = messages
    .slice()
    .sort((firstMessage, secondMessage) => {
      const fstMsgTime = new Date(firstMessage.createdAt).getTime();
      const scndMsgTime = new Date(secondMessage.createdAt).getTime();
      return fstMsgTime - scndMsgTime;
    });

  const messagesElements = sortedMessages.map((message) => {
    if (message.userId === props.userId) {
      return (
        <OwnMessage
          key={message.id}
          id={message.id}
          text={message.text}
          createdAt={message.createdAt}
        />
      );
    }
    return (
      <Message
        key={message.id}
        id={message.id}
        userId={message.userId}
        user={message.user}
        avatar={message.avatar}
        text={message.text}
        createdAt={message.createdAt}
      />
    );
  });

  return (
    <div className="message-list">
      {messagesElements}
      <div className="message-divider"></div>
      <br ref={messagesEndRef} />
    </div>
  );
}

export default MessageList;
