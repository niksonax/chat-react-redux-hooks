import React, { useState } from "react";
import "./Message.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart as faHeartOutlined } from "@fortawesome/free-regular-svg-icons";
import { faHeart as faHeartFilled } from "@fortawesome/free-solid-svg-icons";

function Message(props) {
  const [liked, setLiked] = useState(false);

  const onLikeHandler = () => {
    setLiked(!liked);
  };

  const date = new Date(props.createdAt);
  const time = `${date.getHours()}:${date.getMinutes()}`;

  return (
    <div className="message" id={props.id}>
      <div className="message-user-avatar">
        <img src={props.avatar} alt="avatar" />
      </div>
      <div className="message-text-container">
        <div className="message-header">
          <div className="message-user-name">{props.user}</div>
          <div className="message-time">{time}</div>
        </div>
        <div className="message-text">{props.text}</div>
        <div
          className={liked ? "message-liked" : "message-like"}
          onClick={onLikeHandler}
        >
          {liked ? (
            <FontAwesomeIcon icon={faHeartFilled} />
          ) : (
            <FontAwesomeIcon icon={faHeartOutlined} />
          )}
        </div>
      </div>
    </div>
  );
}

export default Message;
