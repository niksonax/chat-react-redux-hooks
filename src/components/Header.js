import React from "react";
import moment from "moment";
import "./Header.css";

function Header(props) {
  const getLastDate = (messages) => {
    let lastDateTime = 0;
    for (const message of messages) {
      const messageDate = new Date(message.createdAt);
      if (messageDate.getTime() > lastDateTime) {
        lastDateTime = messageDate.getTime();
      }
    }
    const lastDate = new Date(lastDateTime);
    const lastDateFormat = moment(lastDate).format("DD.MM.yyyy HH:mm");
    return lastDateFormat;
  };

  const messages = props.messages;
  const usersCount = props.users.length + 1; // users + current user
  const messagesCount = messages.length;
  const date = getLastDate(messages);

  return (
    <div className="header">
      <div className="header-title">Chat title</div>
      <div className="header-users">
        <span className="header-users-count">{usersCount}</span>
        participants
      </div>
      <div className="header-messages">
        <span className="header-messages-count">{messagesCount}</span>
        messages
      </div>

      <div className="header-last-message-date">{date}</div>
    </div>
  );
}

export default Header;
