import { createReducer } from "@reduxjs/toolkit";
import {
  fetchMessages,
  fetchUsers,
  setUsers,
  setMessages,
  setUser,
  setCurrentUserId,
  removeUser,
  toggleEditModal,
  setEditingMessageId,
} from "../actions/actions";

const initialState = {
  chat: {
    messages: [],
    users: [],
    currentUser: {
      id: 0,
      name: "",
      isAdmin: false,
      isLogined: false,
    },
    editModal: false,
    modal: {
      messageId: 0,
    },
    preloader: true,
  },
};

const rootReducer = createReducer(initialState, (builder) => {
  builder.addCase(fetchMessages.pending, (state) => {
    state.chat.preloader = true;
  });
  builder.addCase(fetchMessages.fulfilled, (state, { payload }) => {
    const { messages } = payload;
    state.chat.messages = messages;
    state.chat.preloader = false;
  });

  builder.addCase(fetchUsers.pending, (state) => {
    state.chat.preloader = true;
  });
  builder.addCase(fetchUsers.fulfilled, (state, { payload }) => {
    const { users } = payload;
    state.chat.users = users;
    state.chat.preloader = false;
  });

  builder.addCase(removeUser.fulfilled, (state, { payload }) => {
    const { userId } = payload;

    console.log("before:", userId, state.chat.users);
    state.chat.users = state.chat.users.filter((user) => user.id !== userId);
    console.log("after:", userId, state.chat.users);
  });

  builder.addCase(setMessages, (state, { payload }) => {
    console.log("setMessages", payload);
    const { messages } = payload;
    console.log(messages);
    state.chat.messages = messages;
  });

  builder.addCase(setUsers, (state, { payload }) => {
    const { users } = payload;
    state.chat.users = users;
  });

  builder.addCase(setUser, (state, { payload }) => {
    const { name, isAdmin, isLogined } = payload;
    state.chat.currentUser.name = name;
    state.chat.currentUser.isAdmin = isAdmin;
    state.chat.currentUser.isLogined = isLogined;
  });

  builder.addCase(setCurrentUserId, (state, { payload }) => {
    const { userId } = payload;
    state.chat.currentUser.id = userId;
  });

  builder.addCase(toggleEditModal, (state, { payload }) => {
    const { isEdit } = payload;
    state.chat.editModal = isEdit;
  });

  builder.addCase(setEditingMessageId, (state, { payload }) => {
    const { messageId } = payload;
    state.chat.modal.messageId = messageId;
  });
});

export default rootReducer;
