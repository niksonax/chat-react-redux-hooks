import { createAction, createAsyncThunk } from "@reduxjs/toolkit";
import { getMessages, getUsers, deleteUser } from "../services/services";

const fetchMessages = createAsyncThunk("FETCH_MESSAGES", async (_args) => ({
  messages: await getMessages(),
}));

const fetchUsers = createAsyncThunk("FETCH_USERS", async (_args) => ({
  users: await getUsers(),
}));

const removeUser = createAsyncThunk("DELETE_USER", async (userId) => {
  await deleteUser(userId);

  return {
    userId,
  };
});

const setUsers = createAction("SET_USERS", (users) => ({
  payload: {
    users: users,
  },
}));

const setMessages = createAction("SET_MESSAGES", (messages) => ({
  payload: {
    messages,
  },
}));

const setUser = createAction("SET_USER", (name, isAdmin, isLogined) => ({
  payload: {
    name,
    isAdmin,
    isLogined,
  },
}));

const setCurrentUserId = createAction("SET_CURRENT_USER_ID", (userId) => ({
  payload: {
    userId,
  },
}));

const toggleEditModal = createAction("TOGGLE_EDIT_MODAL", (isEdit) => ({
  payload: {
    isEdit: isEdit,
  },
}));

const setEditingMessageId = createAction(
  "SET_EDITING_MESSAGE_ID",
  (messageId) => ({
    payload: {
      messageId: messageId,
    },
  })
);

export {
  fetchMessages,
  fetchUsers,
  setUsers,
  setMessages,
  setUser,
  setCurrentUserId,
  removeUser,
  toggleEditModal,
  setEditingMessageId,
};
